import fs from "fs";
import {Transform} from "stream";
// import crypto from "crypto";
// import zlib from "zlib";



const rs = fs.createReadStream('file.txt');

const newFile = fs.createWriteStream('newFile.txt');

/**
 * transform : 实现数据处理/转换逻辑的函数
 * flush : 如果变换进程发出额外的数据，flush方法用于刷新数据。这个参数是可选的
 */
const uppercase = new Transform({
    /**
     * @param chunk 要转换的数据
     * @param encoding 如果输入是String类型，则编码将是String类型。如果是Buffer类型，这个值设置为buffer
     * @param callback (err,transformedChunk)：块处理完后要调用的回调函数。回调函数应该有两个参数——第一个是错误，第二个是转换后的块
     */
    transform(chunk, encoding, callback) {
        callback(null, chunk.toString().toUpperCase());
    }
})
rs
    .pipe(uppercase)
    .pipe(newFile);