import fs from "fs";

const rs = fs.createReadStream('./file.txt');

const run = async () => {
    for await (const chunk of rs) {
        console.log(`Read chunk: ${chunk}`);
    }
    console.log(`No more data.`);
}
run()
