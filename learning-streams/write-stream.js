import fs from "fs";

/**
 * close：当流和流的任何资源已关闭时发出。不会发出更多事件
 * Drain : 当可写流可以恢复写入数据时发出
 * error : 当可写流遇到错误时发出
 * finish : 当可写流结束并且所有写入完成时发出
 * pipe：在可读流上调用stream.pipe()方法时发出
 * unpipe：当在可读流上调用stream.unpipe()方法时发出
 */
const file = fs.createWriteStream('./file.txt');


for (let i = 0; i <= 1000000; i++) {

    file.write("Node.js is a JavaScript runtime built on Google Chrome's V8 JavaScript engine.\n");

}