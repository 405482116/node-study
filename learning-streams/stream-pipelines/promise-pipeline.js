import fs from "fs";
import {pipeline, Transform} from "stream";
import util from "util";

const promisePipeline = util.promisify(pipeline);

const uppercase = new Transform({

    transform(chunk, encoding, callback) {

        // Data processing

        callback(null, chunk.toString().toUpperCase());

    },

});

const run = async () => {
    await promisePipeline(
        fs.createReadStream("./file.txt"),
        uppercase,
        fs.createWriteStream("./newFile.txt"))
    console.log("Pipeline succeeded.");
};
run().catch((err) => {

    console.error("Pipeline failed.", err);

});