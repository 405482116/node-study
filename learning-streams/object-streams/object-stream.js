import {Transform} from "stream";
import {stringify} from "ndjson";

/**
 * @param readableObjectMode : 当为true 时，双工流的可读端处于对象模式
 * @param writableObjectMode : 当为true 时，双工流的可写端处于对象模式
 * @param readableHighWaterMark：为流的可读端配置highWaterMark值
 * @param writableHighWaterMark :为流的可写端配置highWaterMark值
 * @param transform:  实现数据处理/转换逻辑的函数
 */
const Name = new Transform({
    objectMode: true, transform({forename, surname}, encoding, callback) {
        callback(null, {name: `${forename} ${surname}`});
    }
})

Name
    .pipe(stringify())
    .pipe(process.stdout);

Name.write({forename: 'Jason', surname: 'Li'});
Name.write({forename: "Jane", surname: "Doe"});