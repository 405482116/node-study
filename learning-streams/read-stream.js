import fs from "fs";

/**
 * close：当流和流的任何资源已关闭时发出。不会发出更多事件
 * data : 从流中读取新数据时发出
 * end : 读取所有可用数据时发出
 * error : 当可读流遇到错误时发出
 * pause : 可读流暂停时发出
 * readable：当有数据可供读取时发出
 * resume : 当可读流处于暂停状态后恢复时发出
 */

const rs = fs.createReadStream('./file.txt');
rs.on("data", chunk => {
    console.log("-> chunk", chunk.toString());
})
rs.on("end", () => {
    console.log(`No more data`);
})