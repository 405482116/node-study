import {Router} from "express";

const router = new Router();
router.get('/', ((req, res) => {
    const user = req.session.user;
    res.render("index", { user });
}))

export default router;