import express from "express";
import {fileURLToPath} from "url";
import * as path from "path";
import bodyParser from "body-parser";
import session from "express-session";
import index from "./routes/index.mjs";
import auth from "./routes/auth.mjs";
import helmet from "helmet";


const url = import.meta.url;
console.log("-> url", url);
const pathUrl = fileURLToPath(url)
console.log("-> pathUrl", pathUrl);
const __dirname = path.dirname(fileURLToPath(import.meta.url))
console.log("-> __dirname", __dirname);
const PORT = process.env.PORT || 3001;


const views = path.join(__dirname, 'views');
const app = express();
app.set('views', views);
app.set("view engine", "ejs");
app.use(helmet({
    referrerPolicy: {policy: "no-referrer"},
}))
app.use(bodyParser.urlencoded({
    extended: false,
}));
app.use(session({
    name: 'SESSIONID',
    secret: 'node_study',
    resave: false,
    saveUninitialized: false,
    cookie: { sameSite: true },
}))
app.use('/', index);
app.use('/auth', auth);
app.listen(PORT, () => {
    console.log(`Server listening on ${PORT} `);
})