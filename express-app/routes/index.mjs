import { Router } from "express";

const router = Router();
router.get("/", (req, res) => {
  res.render("index", {
    title: "Express with EJS",
  });
});
router.get("/:name?", function (req, res) {
  const title = "Express";
  const name = req.params.name;
  res.send(`<html>
<head>
<title> ${title} </title>
<link rel="stylesheet" href="styles.css">
</head>
<body>
<h1> ${title} </h1>
<p> Welcome to ${title}${name ? `, ${name}.` : ""} </p>
<form method=POST action=data>
Name: <input name=name><input type=submit>
</form>
</body>
</html>

`);
});
router.post("/data", function (req, res) {
  res.redirect(`/${req.body.name}`);
});
// router.get('/', (req, res, next) => {
//     const title = "Express";
//     res.send(`<html><head>
// <title> ${title} </title>
// <link rel="stylesheet" href="styles.css"></head>
// <body>
// <h1> ${title} </h1>
// <p> Welcome to ${title} </p>
// </body>
// </html>
// `);
// })

export default router;
