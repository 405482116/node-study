import express from "express";
import path from "path";
import { fileURLToPath } from "url";
import index from "./routes/index.mjs";
import logger from "./middleware/logger.mjs";
import bodyParser from "body-parser";

const dev = process.env.NODE_ENV !== "production";

if (dev) {
  console.log(`this is dev environment.`);
  // dev specific behaviors here
}
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const PORT = process.env.PORT || 3000;
const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(logger());
app.use(express.static(path.join(__dirname, "public")));
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

app.use("/", index);
app.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}`);
});
