import { MongoClient, ObjectId } from "mongodb";
import { config } from "dotenv";

// https://docs.mongodb.com/drivers/node/current/
config();

const task = process.argv[2];

const URI = `mongodb://${process.env.DB_MONGO_HOST}:${process.env.DB_MONGO_PORT}/`;

MongoClient.connect(

    URI,

    {

        useUnifiedTopology: true,

    },

    connected

);

function connected(err, client) {

    if (err) throw err;
    const tasks = client.db("tasklist").collection("tasks");
    if (task) {
        addTask(client, tasks);
    } else {
        listTasks(client, tasks);
    }
}

function addTask(client, tasks) {
    tasks.insertOne({
        task: task,
    },
        (err) => {
            if (err) throw err;
            console.log("New Task: ", task);
            listTasks(client, tasks);
        }
    );

}

async function listTasks(client, tasks) {
    try {
        const findResult = await tasks.find({_id: new ObjectId('61d91321cb350c47bc94fb22')}).toArray();
        if (!findResult) {
            client.close();
            return;
        }
        console.log('Found documents =>', findResult);
    } catch (error) {

    }
    client.close();
}