import fs from "fs";

const file = './file.txt';
/**
 * BigInt：默认为false；当设置为true 时，从Stats对象返回的数值将被指定为BigInt。BigInt是一个 JavaScript 对象，可让您更可靠地表示更大的数字。
 * Persistent：此值指示 Node.js 进程是否应在文件仍在监视期间继续运行。它默认为true。
 * Interval：间隔值控制应该多久轮询文件以进行更改，以毫秒为单位。当没有提供时间间隔时，默认值为 5,007 毫秒。
 * **/
fs.watchFile(file, {bigint: false, persistent: true, interval: 1000 * 6}, ((curr, prev) => {
    return process.stdout.write(`${file} updated ${curr.mtime}`);
}))