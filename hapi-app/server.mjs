import Hapi from "@hapi/hapi";
import Vision from "@hapi/vision";
import ejs from "ejs";
import {fileURLToPath} from "url";
import path from "path";
import inert from "@hapi/inert";


const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const PORT = process.env.PORT || 3000;

const HOSTNAME = process.env.HOSTNAME || "localhost";

const initialize = async () => {
    // server.route({
    //
    //     method: "GET",
    //
    //     path: "/",
    //
    //     handler: (request, h) => {
    //
    //         return "Welcome to Hapi 12312";
    //
    //     },
    //
    // });
    server.route({

        method: "GET",

        path: "/",

        handler: function (request, h) {

            return h.view("index", {

                title: "Hapi",

            });

        },

    });

    await server.register(Vision);
    await server.register(inert);
    await server.start();
    server.views({
        engines: {
            ejs,
        }, relativeTo: __dirname,

        path: "views",

    });
    server.route({

        method: "GET",

        path: "/file",

        handler: {

            file: path.join(__dirname, "files/file.txt"),

        },

    });
    console.log("Server listening on", server.info.uri);
}
const server = Hapi.server({

    port: PORT,

    host: HOSTNAME,

});
(async () => await initialize())();