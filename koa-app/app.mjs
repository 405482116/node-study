import path from "path";
import Koa from "koa";
import staticServer from "koa-static";
import router from "./routes/index.mjs";
import { fileURLToPath } from "url";
import views from "koa-views";
import logger from "./middleware/logger.mjs";

const PORT = process.env.PORT || 3000;

const dev = process.env.NODE_ENV !== "production";

if (dev) {
  console.log(`this is dev environment.`);
  // dev specific behaviors here
}
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const render = views(path.join(__dirname, "views"), {
  extension: "ejs",
});
const app = new Koa();
app.use(logger());
app.use(staticServer(path.join(__dirname, "public")));
app.use(render);
app.use(router.routes());
app.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}`);
});
