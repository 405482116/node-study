import Router from "@koa/router";

const router = new Router();
router.get("/", async (ctx, next) => {
  ctx.state = {
    title: "Koa.js",
  };

  await ctx.render("index");
});
// router.get('/', async (ctx, next) => {
//     const title = "Koa.js";
//
//     ctx.body = `
//
// <html>
//
// <head>
//
// <title> ${title} </title>
//
// <link rel="stylesheet" href="styles.css"></head>
//
// <body>
//
// <h1> ${title} </h1>
//
// <p> Welcome to ${title} </p>
//
// </body>
//
// </html>
//
// `;
// });
export default router;
