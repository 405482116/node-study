import {MongoClient} from "mongodb";
import express from "express";

import {config} from "dotenv";

config()
const URL = `mongodb://${process.env.DB_MONGO_HOST}:${process.env.DB_MONGO_PORT}/`;

const app = express();

MongoClient.connect(URL, {useUnifiedTopology: true}, (err, client) => {

    if (err) throw err;

    const db = client.db("data");

    const average = db.collection("averages");

    app.get("/", (req, res) => {

        average.findOne({}, (err, data) => {

            if (err) throw err;

            res.send(`Average of all values is ${data.value}.`);

        });

    });

    app.listen(3000);

});