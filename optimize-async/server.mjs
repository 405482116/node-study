import {MongoClient} from "mongodb";
import express from "express";

import {config} from "dotenv";

config()
const URL = `mongodb://${process.env.DB_MONGO_HOST}:${process.env.DB_MONGO_PORT}/`;

const app = express();

MongoClient.connect(URL, {useUnifiedTopology: true}, (err, client) => {

    if (err) throw err;

    const db = client.db("data");

    const values = db.collection("values");

    app.get("/", (req, res) => {

        values.find({}).toArray(function sum(err, data) {

            if (err) {

                res.send(err);

                return;

            }

            // Calculate average

            const average =

                data.reduce((accumulator, value) => accumulator + value.value, 0) /

                data.length;

            res.send(`Average of all values is ${average}.`);

        });

    });

    app.listen(3000);

});