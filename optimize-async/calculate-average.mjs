import {MongoClient} from "mongodb";
import {config} from "dotenv";
config()
const URL = `mongodb://${process.env.DB_MONGO_HOST}:${process.env.DB_MONGO_PORT}/`;

MongoClient.connect(URL, { useUnifiedTopology: true }, (err, client) => {

    if (err) throw err;

    const db = client.db("data");

    const values = db.collection("values");

    const averages = db.collection("averages");

    values.find({}).toArray((err, data) => {

        if (err) throw err;

        // Calculate average

        const average =

            data.reduce((accumulator, value) => accumulator + value.value, 0) /

            data.length;

        averages.find({}).toArray((err) => {

            if (err) throw err;

            averages.insertOne({ value: average }, (err) => {

                if (err) throw err;

                console.log("Stored average in database.");

                client.close();

            });

        });

    });

});