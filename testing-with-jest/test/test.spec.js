const fetch = require('node-fetch');
const uppercase = require('../uppercase.js');
const github = require("../github.js");
const octokitUserData = require("./octokitUserData.js");
jest.mock('node-fetch', () => jest.fn());

describe('github', () => {
    test('Get GitHub user by username', async () => {
        const response = Promise.resolve({
            ok: true,
            json: () => {
                return octokitUserData ? octokitUserData : {};
            },
        });
        fetch.mockImplementation(() => response);
        const githubUser = await github.getGitHubUser("405482116");
        expect(githubUser.id).toEqual(22544417);
        expect(githubUser.login).toEqual('405482116');
        expect(githubUser.name).toEqual(null);
    });

});


describe("uppercase", () => {
    test("uppercase hello returns HELLO", () => {
        expect(uppercase("hello")).toBe("HELLO");
    });
});


describe('mock uppercase', () => {
    test('uppercase hello returns HELLO ', () => {
        const uppercase = jest.fn(() => "HELLO");
        const result = uppercase("hello");

        expect(uppercase).toHaveBeenCalledWith("hello");

        expect(result).toBe("HELLO");
    });
});