process.stdin.on("data", data => {
    const name = data.toString().trim().toUpperCase();
    if (name) {
        process.stdout.write(`hello ${name}`);
    } else  {
        process.stdout.write('input is null');
    }
})