import {WebSocketServer as Server} from 'ws';

const WebSocketServer = new Server({
    port: 3000
});
WebSocketServer.on("connection", (socket) => {
    console.log("connection");
    process.stdin.on("data", data => {
        const str = data.toString();
        socket.send(str);
    })
    socket.on("message", (msg) => {
        console.log("Received:", msg.toString());
        if (msg.toString() === "Hello") socket.send("World!");
    });

});