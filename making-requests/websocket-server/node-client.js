import WebSocket from "ws";


const ws = new WebSocket("ws://localhost:3000");
ws.on("open", () => {
    console.log("Connected");
});
ws.on("close", () => {
    console.log("Disconnected");
});

ws.on("message", (message) => {
    console.log("Received:", message.toString());
});

process.stdin.on("data", data => {
    const str = data.toString();
    ws.send(str);
})