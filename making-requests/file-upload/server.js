import fs from "fs";
import * as http from "http";
import * as path from "path";
import formidable from "formidable";

const HOSTNAME = process.env.HOSTNAME || "0.0.0.0";
const PORT = process.env.PORT || 3000;
const form = fs.readFileSync(path.join(process.cwd(), 'public', 'form.html'));
http
    .createServer((req, res) => {
        if (req.method === "GET") {
            get(res);
            return;
        }
        if (req.method === "POST") {
            post(req, res);
            return;
        }
        error(405, res);
    })

    .listen(PORT, HOSTNAME, () => {
        console.log(`Server listening on port ${PORT}`);
    });

function get(res) {

    res.writeHead(200, {

        "Content-Type": "text/html",

    });

    res.end(form);

};

function post(req, res) {
    if (!/multipart\/form-data/.test(req.headers["content-type"])) {
        error(415, res);
        return;
    }
    const form = formidable({
        multiples: true, uploadDir: "./uploads",
    });

    form.parse(req, (err, fields, files) => {
        if (err) return err;
        res.writeHead(200, {
            "Content-Type": "application/json",
        });
        res.end(JSON.stringify({fields, files,}, null, '\n'));

    });

}

function error(code, res) {

    res.statusCode = code;

    res.end(http.STATUS_CODES[code]);

}