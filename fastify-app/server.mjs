import {fastify as server} from "fastify";
import routes from "./plugins/hello-route.mjs";

const PORT = process.env.PORT || 3000;
const fastify = server();
// fastify.get("/", async (request, reply) => {
//
//     return {message: "Hello world!"};
//
// });
fastify.register(routes);

const startServer = async () => {

    try {

        await fastify.listen(PORT);

        console.log(`server listening on ${fastify.server.address().port}`);

    } catch (err) {

        console.error(err);

        process.exit(1);

    }

};
(async () => await startServer())();