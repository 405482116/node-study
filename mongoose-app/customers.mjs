import mongoose from "mongoose";
import { config } from "dotenv";

// https://mongoosejs.com/docs/guide.html
config();

const URI = `mongodb://${process.env.DB_MONGO_HOST}:${process.env.DB_MONGO_PORT}/`;

mongoose.connect(URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

const Customer = mongoose.model('Customer', {
    firstName: String,
    lastName: String,
})
const custmer = new Customer({
    firstName: 'Meng',
    lastName: 'Li',
})
custmer.save().then(doc => {
    console.log("Added new customer:", doc.firstName, doc.lastName);
    listCustomers();
})

function listCustomers() {
    console.log("Customers:");
    Customer.find({}).then(doc => doc.forEach(customer => {
        console.log(`- ${customer.firstName}, ${customer.lastName}`);
        mongoose.connection.close();
    }))

}