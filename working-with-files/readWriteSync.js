import fs from "fs";
import path from "path";

const filePath = path.join(process.cwd(), 'hello.txt');

const content = fs.readFileSync(filePath, 'utf8');
console.log("-> content", content);
const upperContent = content.toUpperCase();

fs.writeFileSync(filePath, upperContent);
console.log(`file write done.`)
