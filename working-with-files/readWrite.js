import fs from "fs/promises";
import path from "path";

const filePath = path.join(process.cwd(), 'hello.txt');

const run = async () => {
    try {
        const contents = await fs.readFile(filePath, 'utf8');
        console.log("-> contents", contents);
        const lowerContents = contents.toLowerCase();
        await fs.writeFile(filePath, lowerContents);

    } catch (e) {
        process.stderr.write(`read file error : ${e}`)
    }
}
run().then(() => {
    console.log(`write lowerContents done.`);
})