import net from "net";

const HOSTNAME = "localhost";

const PORT = 3000;
net
    .createServer((socket => {
        console.log(`Client connected.`);
        process.stdin.on('data', data => {
            const tmp = data.toString();
            const str = JSON.stringify({name: 'serve', data: tmp})
            socket.write(str);
        });
        socket.on("data", data => {
            const {name, data: oData} = JSON.parse(data.toString())
            console.log(`${name}-> ${oData}`);
        })
    }))
    .listen(PORT, HOSTNAME);