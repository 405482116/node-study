import net from "net";

const HOSTNAME = "localhost";

const PORT = 3000;

const socket = net.connect(PORT, HOSTNAME);
process.stdin.on("data", data => {
    const tmp = data.toString();
    const str = JSON.stringify({name: 'client', data: tmp})
    socket.write(str);

})
socket.on("data", data => {// data is received
    const {name, data: oData} = JSON.parse(data.toString())
    console.log(`${name}->:   ${oData}`);
});
socket.on("close", () => {// socket is closed.
})
socket.on("connect", () => {// the connection is established
});
socket.on("drain", () => {//write buffer ie empty
});
socket.on("end", () => {// the other end of the socket send a FIN packet
});
socket.on("error", err => {// an error occurs
});
socket.on("lookup", (err, address, family, host) => {
    //after resolving the hostname but before connecting on non-UNIX platforms only.
});
socket.on("ready", () => {// the socket is ready to be used.
});
socket.on("timeout", () => {// the socket times out from inactivity.
})