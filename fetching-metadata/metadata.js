import fs from "fs/promises";

const file = process.argv[2];

const printMetadata = async (file) => {
    try {
        const fileStats = await fs.stat(file);
        console.log("-> fileStats", fileStats);
    } catch (e) {
        process.stderr.write(`error:: ${e}`)
    }


};

printMetadata(file).then(() => {
    console.log(`read file stat done.`);
});