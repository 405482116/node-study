import fs from "fs";

const filePath = './file.txt';

fs.chmodSync(filePath, fs.constants.S_IRUSR |
    fs.constants.S_IWUSR |
    fs.constants.S_IWGRP |
    fs.constants.S_IRGRP |
    fs.constants.S_IROTH)
